program : main.o editor.o text_line.o
	g++ -o program main.o editor.o text_line.o
main.o : main.cpp main.h Editor/editor.h TextLine/text_line.h
	g++ -c main.cpp
editor.o : Editor/editor.cpp Editor/editor.h
	g++ -c Editor/editor.cpp
text_line.o : TextLine/text_line.cpp TextLine/text_line.h
	g++ -c TextLine/text_line.cpp
clean :
	rm program main.o editor.o text_line.o